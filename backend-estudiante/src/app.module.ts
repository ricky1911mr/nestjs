import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({// es un json
  imports: [// Aqui se guarda los módulos

  ],
  controllers: [ // Guardaremos los controllers
    AppController
  ],
  providers: [// Guardaremos los services o tambien resolvers
    AppService
  ],
})
export class AppModule {}
