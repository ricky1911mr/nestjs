import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {


  saludar(nombre:string, apellido:string, correo:string):string{
    return `Hola ${nombre} ${apellido} con correo ${correo}`;
  }
}
