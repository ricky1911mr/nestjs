import { Get, Controller, Post, Put, Delete, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('estudiante')  /*ya se va armando y quedaria http://localhost:3000/estudiante el 3000 se puede ver en el main.ts*/
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('hola') /*http://localhost:3000/estudiante/hola*/
  saludo(){
    /*en html return '<h1>' + this.appService.saludar('Ricardo') + '</h1>';*/
  return{
  saludo: this.appService.saludar('Ricardo', 'Paredes', 'andresparedes80@hotmail.com')};
}
@Get('hola/:nombre/:apellido/:correo')
holaSaludoParametroRuta(
  @Param ('nombre') nombreParametro: string,
  @Param ('apellido') apellidoParametro: string,
  @Param ('correo') correoParametro: string){
  return this.appService.saludar(nombreParametro, apellidoParametro, correoParametro);
}
}